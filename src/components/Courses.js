import {Row, Col, Card, Button} from 'react-bootstrap'

export default function Courses () {

	return (

		<Row>
			<Col> 
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Sample Courses</h2>
						</Card.Title>
						<Card.Text>
							<h4>Description:</h4>
							<p>This is single course offering</p>
							<h4>Price:</h4>
							<p>Php: 40,000</p>
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}