import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Courses from '../components/Courses'

export default function Home () {
	return (
		<Fragment>
			<Banner/>
			<Highlights/>
			<Courses/>
		</Fragment>
	)
}